lst = [1,2,3,4,5,6,27,28,29,30,116,117,118]
lst1 = [2,5,6,7,8,21,125,12]
lst2 = [6,7,8,9,0,1,2,3,0]
lst_with_zero = [[],[],[1,2],"",'',12512512]
print(lst)

#1
print('1')
print()
print()

def dbl(a):
    return a*2



print('result: ',list(map(dbl, lst)))
print()
print()
print()

#2
print('2')
print()
print()
print(lst)
print(lst1)
print(lst2)
print()
print()
def um(*args):
	return args[0]*args[1]*args[2]



print('result: ',list(map(um, lst, lst1, lst2)))
print()
print()
print()

#3
print('3')
print(lst)
print()
print()

def ln(*args):
    for element in args:
        element = str(element)
        return len(element)

print('result: ',list(map(ln, lst)))
print()
print()
print()

#4
print('4')
print(lst)
print()
print()
print('result: ', list(filter(lambda x: x % 2 == 0 , lst)))
print()
print()
print()

#5
print('5')
print(lst_with_zero)
print()
print()
print()
print('result: ', list(filter(lambda x: bool(x) == True, lst_with_zero)))
print()
print()
print()

#6
print('6')
print()
print()
print()
print(lst)
print(lst1)
print(lst2)
print()
print()
print()

print('result: ',list(zip(lst, lst1, lst2)))
print()
print()
print()
#7
print('7')
print()
print()
print(lst)
print(lst1)
print()
print('result: ', list(zip(lst, map(dbl, lst1))))









